//
//  CurrencyUiModel.swift
//  CurrencyConverter
//
//  Created by Taher on 30/7/23.
//

import Foundation

struct CurrencyUiModel : Hashable {
    
    let currencyCode : String
    let currencyValue : Double
    let countryName: String
}
