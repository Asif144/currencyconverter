//
//  CurrencyPickerView.swift
//  CurrencyConverter
//
//  Created by Taher on 27/7/23.
//

import SwiftUI
import Combine

struct CurrencyPickerView: View {

    let currencyList: [String]
    @Binding var selectedCurrency: String
    @Binding var lastSelectedCurrency: String
    @FocusState var isFocused: Bool
    let didUpdate: (() -> Void)

    var body: some View {
        HStack {
            Spacer()
            Picker("", selection: $selectedCurrency) {
                ForEach(currencyList, id: \.self) {
                    Text($0)
                        .foregroundColor(.black)
                }
            }
            .onReceive(Just(selectedCurrency)) { value in
                /// TODO: change grid data

                if(lastSelectedCurrency != value) {
                    isFocused = false
                    lastSelectedCurrency = value
                    didUpdate()
                }
            }
            .pickerStyle(.menu)
            .accentColor(.black)
            .overlay(
                RoundedRectangle(cornerRadius: 8.0)
                    .stroke(
                        Color.purpleBorder,
                        lineWidth: 1.0
                    )
            )
            .background(.white)
        }

    }
}
