//
//  CurrencyGridView.swift
//  CurrencyConverter
//
//  Created by Taher on 27/7/23.
//

import SwiftUI

struct CurrencyGridView: View {

    let currencyGridData : [CurrencyUiModel]

    private var gridItemLayout = [
        GridItem(.flexible(), spacing: 12.0),
        GridItem(.flexible(), spacing: 12.0),
        GridItem(.flexible(), spacing: 0.0)
    ]

    var body: some View {
        ScrollView {
            Spacer(minLength: 4.0)
            LazyVGrid(columns: gridItemLayout, spacing: 12.0) {
                ForEach(currencyGridData, id: \.self) { data in
                    VStack {
                        Text("\(data.currencyCode)")
                            .foregroundColor(.black)
                            .font(.system(size: 12.0))
                        Text(data.currencyValue.currencyFormatted)
                            .foregroundColor(.black)
                            .font(.system(size: 16.0))
                        Text("\(data.countryName)")
                            .foregroundColor(.gray)
                            .font(.system(size: 10.0))
                            .lineLimit(2)
                    }
                    .padding()
                    .frame(height: 120.0)
                    .frame(maxWidth: .infinity)
                    .overlay(
                        RoundedRectangle(cornerRadius: 8.0)
                            .stroke(
                                Color.purpleBorder,
                                lineWidth: 1.0
                            )
                    )
                }
            }
            .padding(
                EdgeInsets(top: 0.0, leading: 16.0, bottom: 8.0, trailing: 16.0)
            )
        }
    }

    init(currencyGridData: [CurrencyUiModel]) {
        self.currencyGridData = currencyGridData
    }
}

