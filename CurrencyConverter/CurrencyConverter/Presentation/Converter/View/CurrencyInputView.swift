//
//  CurrencyInputView.swift
//  CurrencyConverter
//
//  Created by Taher on 27/7/23.
//

import SwiftUI

struct CurrencyInputView: View {

    @Binding var numberText: String
    let validationErrorText: String
    @FocusState var isFocused: Bool
    let didUpdate: (() -> Void)

    var body: some View {
        TextField("insert amount", text: $numberText)
            .accentColor(Color.purpleBorder)
            .multilineTextAlignment(.trailing)
            .padding()
            .frame(maxWidth: .infinity)
            .frame(height: 48.0)
            .background(.white)
            .foregroundColor(.black)
            .focused($isFocused)
            .onTapGesture {
                isFocused = true
            }
            .overlay(
                RoundedRectangle(cornerRadius: 8.0)
                    .stroke(
                        validationErrorText.isEmpty ? Color.purpleBorder : .red,
                        lineWidth: 1.0
                    )
            )
            .onChange(of: numberText, perform: { _ in
                didUpdate()
            })
    }
}

