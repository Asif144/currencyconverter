//
//  AlertView.swift
//  CurrencyConverter
//
//  Created by Taher on 31/7/23.
//

import SwiftUI

struct AlertView: View {

    let message : String
    let didTap: (() -> Void)

    var body: some View {
        VStack (spacing: 0){
            Text(message)
                .foregroundColor(.red)
                .font(.body)
                .padding(16.0)
                .frame(maxWidth: 300,  maxHeight: 100)
                .background(.white)
                .clipShape(
                    RoundedCornerShape(
                        corners: [.topLeft, .topRight], radius: 10.0
                    )
                )

            Button {
                didTap()
            } label: {
                Text(LocalizedString.ok)
                    .foregroundColor(.white)
                    .frame(maxWidth: 300, maxHeight: 30)
            }
            .frame(maxWidth: 300, maxHeight: 30)
            .background(LinearGradient(
                colors: [.red, .black],
                startPoint: .topLeading,
                endPoint: .bottomTrailing
            ))
            .clipShape(
                RoundedCornerShape(
                    corners: [.bottomLeft, .bottomRight], radius: 10.0
                )
            )
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(.gray.opacity(0.4))
    }
}
