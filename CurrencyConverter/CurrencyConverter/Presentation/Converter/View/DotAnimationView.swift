//
//  DotAnimationView.swift
//  CurrencyConverter
//
//  Created by Taher on 29/7/23.
//

import SwiftUI

struct DotAnimationView: View {

    @State private var leftOffset: CGFloat = 0.0
    @State private var rightOffset: CGFloat = 120.0

    var body: some View {
        VStack {
            ZStack {
                circle
                    .animation(
                        Animation.easeInOut(duration: 1.0),
                        value: leftOffset
                    )
                circle
                    .animation(
                        Animation.easeInOut(duration: 1.0).delay(0.2),
                        value: leftOffset
                    )
                circle
                    .animation(
                        Animation.easeInOut(duration: 1.0).delay(0.4),
                        value: leftOffset
                    )
            }
            .onReceive(animationTimer) { (_) in
                swap(&self.leftOffset, &self.rightOffset)
            }
        }
        .onAppear {
            leftOffset = -120.0
        }
    }

    private var circle: some View {
        Circle()
            .fill(Color.purpleBorder)
            .frame(width: 20.0, height: 20.0)
            .offset(x: leftOffset)
            .opacity(0.6)
    }

    private let animationTimer = Timer.publish(
        every: 1.8,
        tolerance: 0.5,
        on: .main,
        in: .common
    )
    .autoconnect()
}
