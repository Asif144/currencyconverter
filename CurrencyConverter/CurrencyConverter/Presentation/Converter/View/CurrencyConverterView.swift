//
//  ContentView.swift
//  CurrencyConverter
//
//  Created by Taher on 26/7/23.
//

import SwiftUI

struct CurrencyConverterView: View {

    @ObservedObject private var viewModel: CurrencyConverterViewModel

    var body: some View {
        VStack {
            switch viewModel.currentState {
            case .splash:
                VStack(spacing: 0) {
                    screenTitle
                    DotAnimationView()
                }
                .frame(maxWidth: .infinity, maxHeight: .infinity)
            case .loading:
                VStack {
                    DotAnimationView()
                }
                .frame(maxWidth: .infinity, maxHeight: .infinity)
            case .loaded(let message):
                ZStack {
                    currencyConverterView.disabled(!message.isEmpty)
                    if(!message.isEmpty) {
                        AlertView(
                            message: message,
                            didTap: viewModel.didTapOkButton
                        )
                    }
                }
            }
        }
        .background(.white)
        .preferredColorScheme(.light)
    }

    init(viewModel: CurrencyConverterViewModel) {
        self.viewModel = viewModel
    }

    @State private var numberText = ""
    @State private var selectedCurrency = AppConstant.defaultCurrency
    @State private var lastSelectedCurrency = AppConstant.defaultCurrency
    @FocusState private var isFocused : Bool

    private var screenTitle: some View {
        Text(LocalizedString.appTitle)
            .foregroundColor(.black)
            .font(.system(size: 24.0, weight: .bold))
            .padding()
    }

    private var textFieldError: some View {
        HStack {
            Spacer()
            Text(viewModel.validationErrorText)
                .foregroundColor(.red)
                .font(.system(size: 12.0))
                .padding(getEdgeInset(bottom: 0.0))
        }
    }

    private var currencyConverterView: some View {
        VStack(alignment: .leading) {
            screenTitle
            /// text field
            CurrencyInputView(
                numberText: $numberText,
                validationErrorText: viewModel.validationErrorText,
                isFocused: _isFocused,
                didUpdate: didUpdateCurrencyInput
            )
            .padding(getEdgeInset(bottom: 0.0))

            if !viewModel.validationErrorText.isEmpty {
                textFieldError
            }
            VStack {
                /// drop down
                CurrencyPickerView(
                    currencyList: viewModel.currencyCodeList,
                    selectedCurrency: $selectedCurrency,
                    lastSelectedCurrency: $lastSelectedCurrency,
                    isFocused: _isFocused,
                    didUpdate: didUpdateCurrencyCode
                )
                .padding(getEdgeInset())

                /// GRID
                CurrencyGridView(currencyGridData: viewModel.currencyUIDataList)
            }
            .background(.white)
            .onTapGesture {
                isFocused = false;
            }
        }
    }

    private func didUpdateCurrencyInput() {
        viewModel.didUpdateInputText(inputText: numberText)
    }

    private func didUpdateCurrencyCode() {
        viewModel.didChangeCurrencyCode(newCode: selectedCurrency)
    }

    private func getEdgeInset(
        top: CGFloat = 0.0,
        leading: CGFloat = 16.0,
        bottom: CGFloat = 8.0,
        trailing: CGFloat = 16.0
    ) -> EdgeInsets {
        return EdgeInsets(
            top: top,
            leading: leading,
            bottom: bottom,
            trailing: trailing
        )
    }
}
