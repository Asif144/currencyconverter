//
//  CurrencyConverterViewModel.swift
//  CurrencyConverter
//
//  Created by Taher on 27/7/23.
//

import SwiftUI
import Combine
import Network

final class CurrencyConverterViewModel: ObservableObject {

    @Published private(set) var validationErrorText = ""
    @Published private(set) var currencyUIDataList = [CurrencyUiModel]()
    @Published private(set) var currentState : CurrencyConverterViewModelState
    @Published private(set) var currencyCodeList : [String] = []

    private let numberValidator: NumberValidator
    private let localStorageUseCase: LocalStorageUseCaseProtocol
    private let getCurrencyApiUseCase: GetCurrencyApiUseCaseProtocol
    private let timerUseCase: TimerUseCaseProtocol
    private let currencyConversionUseCase: CurrencyConversionUseCaseProtocol
    private let staticDataUseCase: StaticDataUseCaseProtocol
    private var networkPathMonitorUseCase: NetworkPathMonitorUseCaseProtocol
    private var cancellable = Set<AnyCancellable>()
    private var currencyInputValue : Double = 0.0
    private var currencyCode : String = AppConstant.defaultCurrency
    private var currencyToCountryMap : [String : String] = [:]
    private var currencyEntities : [CurrencyEntity] = []
    private var isInternetConnected : Bool = false
    private let networkMonitorQueue = DispatchQueue(label: "MonitorQueue")
    
    init(
        numberValidator: NumberValidator,
        currentState: CurrencyConverterViewModelState,
        localStorageUseCase: LocalStorageUseCaseProtocol,
        getCurrencyApiUseCase: GetCurrencyApiUseCaseProtocol,
        timerUseCase: TimerUseCaseProtocol,
        currencyConversionUseCase: CurrencyConversionUseCaseProtocol,
        staticDataUseCase: StaticDataUseCaseProtocol,
        networkPathMonitorUseCase: NetworkPathMonitorUseCaseProtocol
    ) {
        self.numberValidator = numberValidator
        self.localStorageUseCase = localStorageUseCase
        self.getCurrencyApiUseCase = getCurrencyApiUseCase
        self.timerUseCase = timerUseCase
        self.currencyConversionUseCase = currencyConversionUseCase
        self.staticDataUseCase = staticDataUseCase
        self.networkPathMonitorUseCase = networkPathMonitorUseCase
        self.currentState = currentState
        handleNetworkStatus()
        currencyCodeList = staticDataUseCase.getAllCurrencyCode()
        currencyToCountryMap = staticDataUseCase.getCurrencyToCountryMap()
    }

    func didUpdateInputText(inputText : String) {
        guard checkNumberValidation(inputText: inputText) else {
            updateWithEmptyData()
            return
        }
        guard let newValue = Double(inputText) else {
            updateWithEmptyData()
            return
        }
        currencyInputValue = newValue
        updateUIData()
        fetchAndUpdateData()
    }

    func didChangeCurrencyCode(newCode : String) {
        currencyCode = newCode
        updateUIData()
        fetchAndUpdateData()
    }

    func didTapOkButton() {
        currentState = .loaded("")
    }

    private func handleNetworkStatus() {
        networkPathMonitorUseCase.pathUpdateHandler = { [weak self] path in
            guard let owner = self else { return }
            DispatchQueue.main.async {
                NSLog("NETWORK Status: \(path)")
                if(path == .satisfied) {
                    owner.isInternetConnected = true
                    owner.fetchAndUpdateData()
                } else {
                    owner.isInternetConnected = false
                    guard owner.timerUseCase.isValidTimeToFetchData() else {
                        owner.handleErrorState(errorMessage: "")
                        return
                    }
                    owner.handleErrorState(errorMessage: LocalizedString.noInternet)
                }
            }
        }
        networkPathMonitorUseCase.start(queue: networkMonitorQueue)
    }

    private func updateWithEmptyData() {
        currencyInputValue = 0
        updateUIData()
    }

    private func updateUIData() {
        guard let baseCurrency = getBaseCurrency(currencyCode: currencyCode) else {
            currentState = .loaded(LocalizedString.currencyNotFound(currencyCode))
            return
        }
        currencyUIDataList = currencyConversionUseCase.getNewCurrencyList(
            inputNumber: currencyInputValue,
            baseCurrency: baseCurrency,
            allCurrency: getCurrencyEntities(),
            currencyToCountryMap: currencyToCountryMap
        )
        currentState = .loaded("")
    }

    private func getCurrencyEntities() -> [CurrencyEntity] {
        if currencyEntities.isEmpty {
            currencyEntities = localStorageUseCase.getEntityList()
        }
        return currencyEntities
    }

    private func getBaseCurrency(currencyCode: String) -> CurrencyUiModel? {
        let entities = getCurrencyEntities()
        guard let baseCurrency = entities.first(where: { $0.currencyCode == currencyCode }) else {
            return nil
        }
        return CurrencyUiModel(
            currencyCode: currencyCode,
            currencyValue: baseCurrency.currencyValue,
            countryName: staticDataUseCase.getCountryName(for: currencyCode)
        )
    }

    private func fetchAndUpdateData() {
        guard timerUseCase.isValidTimeToFetchData() else {
            fetchDataFromLocalStorage()
            updateUIData()
            return
        }
        fetchDataFromApi()
    }

    private func fetchDataFromApi() {
        if currentState != .splash {
            currentState = .loading
        }
        guard isInternetConnected else {
            handleErrorState(errorMessage: LocalizedString.noInternet)
            return
        }
        getCurrencyApiUseCase
            .getApiData()
            .receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { [weak self] completion in
                    guard let owner = self else { return }
                    switch completion {
                    case let .failure(error):
                        NSLog("ERROR: \(error.localizedDescription)")
                        owner.handleErrorState(errorMessage: LocalizedString.invalidResponse)
                    case .finished:
                        NSLog("Finished api data fetching")
                    }
                },
                receiveValue: { [weak self] data in
                    guard let owner = self else { return }
                    owner.processApiData(data: data)
                    owner.currentState = .loaded("")
                }
            )
            .store(in: &cancellable)
    }

    private func handleErrorState(errorMessage : String) {
        fetchDataFromLocalStorage()
        updateUIData()
        currentState = .loaded(errorMessage)
    }

    private func processApiData(data: CurrencyApiModel) {
        currencyUIDataList = currencyConversionUseCase.getConvertedUIModel(
            currencyApiData: data,
            currencyToCountryMap: currencyToCountryMap
        )
        guard !currencyUIDataList.isEmpty else {
            return
        }
        timerUseCase.saveDataUpdateTime(lastUpdateTimeIntervalInSeconds: nil)
        localStorageUseCase.clearStorage()
        localStorageUseCase.saveEntity(entities: data.rates)
        currencyEntities = localStorageUseCase.getEntityList()
        currencyCodeList = Array(data.rates.keys).sorted { $0 < $1}
        updateUIData()
    }

    private func fetchDataFromLocalStorage() {
        if(!currencyEntities.isEmpty) {
            return
        }
        currencyEntities = localStorageUseCase.getEntityList()
        currencyCodeList = currencyEntities.map { $0.currencyCode ?? "" }.sorted()
        currencyUIDataList.removeAll()
        for entity in currencyEntities {
            currencyUIDataList.append(
                CurrencyUiModel(
                    currencyCode: entity.currencyCode ?? "",
                    currencyValue: entity.currencyValue,
                    countryName: staticDataUseCase.getCountryName(for: entity.currencyCode ?? "")
                )
            )
        }
    }

    private func checkNumberValidation(inputText : String) -> Bool {
        let trimmedText = inputText.trimmingCharacters(in: .whitespaces)
        guard !trimmedText.isEmpty else {
            validationErrorText = LocalizedString.insertValidPositiveNumber
            return false
        }
        guard numberValidator.isValidLength(number: trimmedText) else {
            validationErrorText = LocalizedString.atMostCharacters(numberValidator.maxLength)
            return false
        }
        guard numberValidator.isValidPositiveNumber(number: trimmedText) else {
            validationErrorText = LocalizedString.insertValidPositiveNumber
            return false
        }
        validationErrorText = ""
        return true
    }

    deinit {
        networkPathMonitorUseCase.cancel()
    }
}

