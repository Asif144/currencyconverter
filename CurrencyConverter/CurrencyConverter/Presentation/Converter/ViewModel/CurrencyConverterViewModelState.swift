//
//  CurrencyConverterViewModelState.swift
//  CurrencyConverter
//
//  Created by Taher on 27/7/23.
//

import Foundation

enum CurrencyConverterViewModelState: Equatable {
    
    case splash
    case loading
    case loaded(String)
}
