//
//  PersistenceController.swift
//  CurrencyConverter
//
//  Created by Taher on 29/7/23.
//

import CoreData

struct PersistenceController {

    static let shared = PersistenceController()

    let container: NSPersistentContainer

    static var testPersistenceController: PersistenceController = {
        let controller = PersistenceController(inMemory: true)
        return controller
    }()

    init(inMemory: Bool = false, modelName: String = AppConstant.coreDataModelName) {
        container = NSPersistentContainer(name: modelName)
        container.viewContext.mergePolicy = NSMergePolicy(
            merge: .mergeByPropertyObjectTrumpMergePolicyType
        )
        if inMemory {
            container.persistentStoreDescriptions.first?.url = URL(fileURLWithPath: "/dev/null")
        }
        container.loadPersistentStores { description, error in
            if let error = error {
                fatalError("Error: \(error.localizedDescription)")
            }
        }
    }
}
