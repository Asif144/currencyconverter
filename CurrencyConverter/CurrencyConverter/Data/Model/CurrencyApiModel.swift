//
//  File.swift
//  CurrencyConverter
//
//  Created by Taher on 29/7/23.
//

import Foundation

struct CurrencyApiModel: Codable {
    
    let timestamp : Int
    let base : String
    let rates : [String: Double]
}

extension CurrencyApiModel {

    static func createDummy(
        timeStamp : Int = 1,
        base : String = "USD",
        rates : [String: Double]
    ) -> CurrencyApiModel {
        CurrencyApiModel(
            timestamp: timeStamp,
            base: base,
            rates: rates
        )
    }
}
