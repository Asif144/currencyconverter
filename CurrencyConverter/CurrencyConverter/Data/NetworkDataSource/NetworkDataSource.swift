//
//  NetworkDataSource.swift
//  CurrencyConverter
//
//  Created by Taher on 29/7/23.
//

import Combine
import Foundation

final class NetworkDataSource: NetworkDataSourceProtocol {

    private let decoder: JSONDecoder
    private let session: URLSession

    init(decoder: JSONDecoder = JSONDecoder(), session: URLSession = .shared) {
        self.decoder = decoder
        self.session = session
    }

    func performRequest<T>(_ configuration: RequestConfiguration, for type: T.Type) -> Observable<T>
        where T: Decodable {
        return request(
            configuration: configuration,
            decoder: decoder,
            session: session
        )
    }
}
