//
//  APIRequestConfigurations.swift
//  CurrencyConverter
//
//  Created by Taher on 29/7/23.
//

import Foundation

import Foundation

enum APIRequestConfigurations: RequestConfiguration {

    case currencyData
}

extension APIRequestConfigurations {

    var endpoint: String {
        switch self {
        case .currencyData: return "latest.json"
        }
    }

    var method: URLRequest.HttpMethod {
        switch self {
        case .currencyData: return .get
        }
    }

    var parameters: HTTPRequestParameters? {
        switch self {
        case .currencyData: return nil
        }
    }

    var retryCount: Int {
        switch self {
        case .currencyData: return 0
        }
    }

    var headers: HTTPHeaders {
        [
            "accept": "application/json",
            "Authorization": "Token \(AppConfiguration.apiAppId)"
        ]
    }
}
