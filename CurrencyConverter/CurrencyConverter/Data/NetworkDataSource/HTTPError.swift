//
//  HTTPError.swift
//  CurrencyConverter
//
//  Created by Taher on 29/7/23.
//

import Foundation

enum HTTPError: Error, Equatable {

    case statusCode(Int)
    case invalidResponse
    case invalidUrl
}

extension HTTPError: LocalizedError {

    var errorDescription: String {

        switch self {
        case .invalidResponse:
            return LocalizedString.invalidResponse
        case let .statusCode(int):
            return String(int)
        case .invalidUrl:
            return LocalizedString.invalidURL
        }
    }
}
