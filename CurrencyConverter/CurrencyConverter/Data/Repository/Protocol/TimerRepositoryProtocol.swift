//
//  TimerRepositoryProtocol.swift
//  CurrencyConverter
//
//  Created by Taher on 29/7/23.
//

import Foundation

protocol TimerRepositoryProtocol {

    func saveInterval(key: String, value: Int)
    func getInterval(key: String) -> Int?
    func deleteInterval(key: String)
}
