//
//  CurrencyApiRepositoryProtocol.swift
//  CurrencyConverter
//
//  Created by Taher on 29/7/23.
//

import Combine

protocol CurrencyApiRepositoryProtocol: AnyObject {

    func getCurrencyData() -> Observable<CurrencyApiModel>
}
