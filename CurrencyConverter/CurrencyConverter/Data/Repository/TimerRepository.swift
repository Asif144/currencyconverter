//
//  TimerRepository.swift
//  CurrencyConverter
//
//  Created by Taher on 29/7/23.
//

import Foundation

final class TimerRepository: TimerRepositoryProtocol {

    let storage: UserDefaults

    init(storage: UserDefaults = UserDefaults.standard) {
        self.storage = storage
    }

    func saveInterval(key: String, value: Int) {
        storage.set(value, forKey: key)
    }

    func getInterval(key: String) -> Int? {
        guard let value = storage.object(forKey: key),
              let intervalValue = value as? Int else {
            return nil
        }
        return intervalValue
    }

    func deleteInterval(key: String) {
        storage.removeObject(forKey: key)
    }
}
