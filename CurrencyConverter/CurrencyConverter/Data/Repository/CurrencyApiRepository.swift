//
//  CurrencyRepository.swift
//  CurrencyConverter
//
//  Created by Taher on 29/7/23.
//

import Combine
import Foundation

final class CurrencyApiRepository: CurrencyApiRepositoryProtocol {

    let network: NetworkDataSourceProtocol

    init(network: NetworkDataSourceProtocol) {
        self.network = network
    }

    func getCurrencyData() -> Observable<CurrencyApiModel> {
        network.performRequest(
            APIRequestConfigurations.currencyData,
            for: CurrencyApiModel.self
        )
    }
}
