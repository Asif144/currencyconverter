//
//  Constant.swift
//  CurrencyConverter
//
//  Created by Taher on 28/7/23.
//

import Foundation

enum AppConstant {
    
    static let defaultCurrency = "USD"
    static let logoImage = "currency_convert_symbol"
    static let minimumIntervalTimeInMinute = 30
    static let intervalStorageKey = "intervalStorageKey"
    static let coreDataModelName = "CurrencyDataLocalStorage"
}
