//
//  StringKey.swift
//  CurrencyConverter
//
//  Created by Taher on 29/7/23.
//

import Foundation

enum LocalizedString {

    static let appTitle = NSLocalizedString("appTitle", comment: "")
    static let insertValidPositiveNumber = NSLocalizedString("insertValidPositiveNumber", comment: "")
    static let ok = NSLocalizedString("ok", comment: "")
    static let invalidResponse = NSLocalizedString("invalidResponse", comment: "")
    static let invalidURL = NSLocalizedString("invalidURL", comment: "")
    static let noDataFound = NSLocalizedString("noDataFound", comment: "")
    static let noInternet = NSLocalizedString("noInternet", comment: "")

    static func atMostCharacters(_ value: Int) -> String {
        return String(
            format: NSLocalizedString("atMost %@ characters", comment: ""),
            "\(value)"
        )
    }

    static func currencyNotFound(_ value: String) -> String {
        return String(
            format: NSLocalizedString("%@ currency data not found", comment: ""),
            "\(value)"
        )
    }
}
