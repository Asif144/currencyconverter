//
//  CurrencyConverterApp.swift
//  CurrencyConverter
//
//  Created by Taher on 26/7/23.
//

import SwiftUI

@main
struct CurrencyConverterApp: App {

    let networkDataSource: NetworkDataSourceProtocol
    let currencyApiRepository: CurrencyApiRepositoryProtocol
    let localStorageUseCase: LocalStorageUseCaseProtocol
    let getCurrencyApiUseCase: GetCurrencyApiUseCaseProtocol
    let staticDataUseCase: StaticDataUseCaseProtocol
    let networkPathMonitorUseCase: NetworkPathMonitorUseCaseProtocol

    var body: some Scene {
        WindowGroup {
            CurrencyConverterView(
                viewModel: CurrencyConverterViewModel(
                    numberValidator: NumberValidator(),
                    currentState: .splash,
                    localStorageUseCase: localStorageUseCase,
                    getCurrencyApiUseCase: getCurrencyApiUseCase,
                    timerUseCase: TimerUseCase(timerRepository: TimerRepository()),
                    currencyConversionUseCase: CurrencyConversionUseCase(),
                    staticDataUseCase: staticDataUseCase,
                    networkPathMonitorUseCase: networkPathMonitorUseCase
                )
            )
        }
    }
    init() {
        networkDataSource = NetworkDataSource()
        currencyApiRepository = CurrencyApiRepository(network: networkDataSource)
        localStorageUseCase = LocalStorageUseCase(persistenceController: PersistenceController.shared)
        getCurrencyApiUseCase = GetCurrencyApiUseCase(repository: currencyApiRepository)
        staticDataUseCase = StaticDataUseCase()
        networkPathMonitorUseCase = NetworkPathMonitorUseCase()
    }
}
