//
//  AppConfiguration.swift
//  CurrencyConverter
//
//  Created by Taher on 29/7/23.
//

import Foundation

enum AppConfiguration: String {

    case debug
    case release

    // MARK: - Current Configuration

    static let current: AppConfiguration = {
        #if DEBUG
            return .debug
        #elseif RELEASE
            return .release
        #endif
    }()

    static var baseCurrencyApiURL: String {
        guard let urlValue = Bundle.main.infoDictionary?["CURRENCY_API_BASE_URL"] as? String else {
            NSLog("Error: didn't find url in configuration file")
            return ""
        }
        return "https://\(urlValue)/api/"
    }

    static var apiAppId: String {
        guard let appId = Bundle.main.infoDictionary?["CURRENCY_API_APP_ID"] as? String else {
            fatalError("Error: didn't find Client Id in configuration file")
        }
        return appId
    }
}
