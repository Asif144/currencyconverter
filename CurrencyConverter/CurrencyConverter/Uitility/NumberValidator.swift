//
//  NumberValidator.swift
//  CurrencyConverter
//
//  Created by Taher on 26/7/23.
//

import Foundation

struct NumberValidator: NumberValidatorProtocol {

    var maxLength: Int { 7 }

    func isValidLength(number: String) -> Bool {
        return !number.isEmpty && number.count <= maxLength
    }

    func isValidPositiveNumber(
        number: String,
        pattern: String = #"^(0*[1-9]\d*(\.\d+)?|0*\.\d+)$"#
    ) -> Bool {
        do{
            let regex = try NSRegularExpression(pattern: pattern, options: [])
            let matches = regex.matches(
                in: number,
                options: [],
                range: NSRange(location: 0, length: number.count)
            )
            return !matches.isEmpty
        } catch (let error){
            NSLog(error.localizedDescription)
        }
        return false
    }
}
