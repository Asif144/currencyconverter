//
//  NumberValidatorProtocol.swift
//  CurrencyConverter
//
//  Created by Taher on 26/7/23.
//

import Foundation

protocol NumberValidatorProtocol {

    var maxLength: Int { get }
    func isValidLength(number: String) -> Bool
    func isValidPositiveNumber(number: String, pattern: String) -> Bool
}
