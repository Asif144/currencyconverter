//
//  NetworkPathMonitorUseCase.swift
//  CurrencyConverter
//
//  Created by Taher on 1/8/23.
//

import Network
import Foundation
final class NetworkPathMonitorUseCase : NetworkPathMonitorUseCaseProtocol {

    var currentPath: NWPath
    var pathUpdateHandler: ((NWPath.Status) -> Void)?
    let monitor : NWPathMonitor
    var isFirstTime = true

    init(monitor : NWPathMonitor = NWPathMonitor()) {
        self.monitor = monitor
        currentPath = monitor.currentPath
        monitor.pathUpdateHandler = { [weak self] path in
            guard let owner = self else { return }
            if owner.isFirstTime || owner.currentPath.status !=  path.status {
                owner.pathUpdateHandler?(path.status)
                owner.currentPath = path
                owner.isFirstTime = false
            }
        }
    }

    func start(queue: DispatchQueue) {
        monitor.start(queue: queue)
    }

    func cancel() {
        monitor.cancel()
    }
}
