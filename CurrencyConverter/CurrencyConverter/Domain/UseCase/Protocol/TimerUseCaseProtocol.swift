//
//  TimerUseCaseProtocol.swift
//  CurrencyConverter
//
//  Created by Taher on 29/7/23.
//

import Foundation

protocol TimerUseCaseProtocol {

    var minimumIntervalTimeInMinute: Int { get }
    var intervalStorageKey: String { get }
    func isValidTimeToFetchData() -> Bool
    func saveDataUpdateTime(lastUpdateTimeIntervalInSeconds: Int?)
    func deleteDataUpdateTime()
}
