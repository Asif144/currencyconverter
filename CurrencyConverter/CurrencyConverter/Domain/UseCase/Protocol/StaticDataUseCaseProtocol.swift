//
//  StaticDataUseCaseProtocol.swift
//  CurrencyConverter
//
//  Created by Taher on 30/7/23.
//

import Foundation

protocol StaticDataUseCaseProtocol {

    func getCountryName(for currencyCode: String) -> String
    func getAllCurrencyCode() -> [String]
    func getCurrencyToCountryMap() -> [String : String]
}
