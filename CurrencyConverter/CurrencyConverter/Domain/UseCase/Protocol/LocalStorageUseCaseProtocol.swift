//
//  LocalStorageUseCaseProtocol.swift
//  CurrencyConverter
//
//  Created by Taher on 29/7/23.
//

import Foundation

protocol LocalStorageUseCaseProtocol {

    func saveEntity(entities: CurrencyEntityType)
    func getEntityList() -> [CurrencyEntity]
    func clearStorage()
}
