//
//  GetCurrencyApiUseCaseProtocol.swift
//  CurrencyConverter
//
//  Created by Taher on 29/7/23.
//

import Combine
import Foundation

protocol GetCurrencyApiUseCaseProtocol: AnyObject {

    func getApiData() -> Observable<CurrencyApiModel>
}
