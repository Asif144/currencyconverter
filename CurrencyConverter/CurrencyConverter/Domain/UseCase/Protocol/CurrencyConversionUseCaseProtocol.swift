//
//  CurrencyConversionUseCaseProtocol.swift
//  CurrencyConverter
//
//  Created by Taher on 29/7/23.
//

import Foundation

protocol CurrencyConversionUseCaseProtocol {

    func getNewCurrencyList(
        inputNumber: Double,
        baseCurrency: CurrencyUiModel,
        allCurrency: [CurrencyEntity],
        currencyToCountryMap: [String : String]
    ) -> [CurrencyUiModel]

    func getConvertedUIModel(
        currencyApiData: CurrencyApiModel,
        currencyToCountryMap: [String : String]
    ) -> [CurrencyUiModel]
}
