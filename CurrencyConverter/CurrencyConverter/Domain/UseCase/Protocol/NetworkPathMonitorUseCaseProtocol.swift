//
//  NetworkPathMonitorUseCaseProtocol.swift
//  CurrencyConverter
//
//  Created by Taher on 31/7/23.
//

import Network

protocol NetworkPathMonitorUseCaseProtocol {

    var pathUpdateHandler: ((_ newPath: NWPath.Status) -> Void)? { get set}
    func start(queue: DispatchQueue)
    func cancel()
}
