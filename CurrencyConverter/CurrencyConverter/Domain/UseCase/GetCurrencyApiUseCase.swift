//
//  GetCurrencyApiUseCase.swift
//  CurrencyConverter
//
//  Created by Taher on 29/7/23.
//

import Combine
import Foundation

final class GetCurrencyApiUseCase: GetCurrencyApiUseCaseProtocol {

    private let currencyRepository: CurrencyApiRepositoryProtocol

    init(repository: CurrencyApiRepositoryProtocol) {
        currencyRepository = repository
    }

    func getApiData() -> Observable<CurrencyApiModel> {
        currencyRepository.getCurrencyData()
    }
}
