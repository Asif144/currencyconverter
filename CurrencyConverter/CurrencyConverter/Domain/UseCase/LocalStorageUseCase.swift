//
//  LocalStorageUseCase.swift
//  CurrencyConverter
//
//  Created by Taher on 30/7/23.
//

import CoreData

final class LocalStorageUseCase: LocalStorageUseCaseProtocol {

    private let coreDataContext: NSManagedObjectContext
    private var currencyList: [CurrencyEntity] = []

    init(persistenceController: PersistenceController) {
        coreDataContext = persistenceController.container.newBackgroundContext()
        coreDataContext.automaticallyMergesChangesFromParent = true
    }

    func saveEntity(entities: CurrencyEntityType) {
        NSLog("XYZ SAVE ENTITY START")
        for (currencyCode, currencyValue) in entities {
            let newEntity = CurrencyEntity(context: coreDataContext)
            newEntity.currencyCode = currencyCode
            newEntity.currencyValue = currencyValue
        }
        coreDataContext.performAndWait {
            if coreDataContext.hasChanges {
                do {
                    try coreDataContext.save()
                } catch {
                    NSLog(error.localizedDescription)
                }
                NSLog("XYZ SAVE ENTITY SAVED")
            }
        }
        NSLog("XYZ SAVE ENTITY END")
    }

    func getEntityList() -> [CurrencyEntity] {
        do {
            currencyList = try coreDataContext.fetch(CurrencyEntity.fetchRequest())
            NSLog("XYZ entity count: \(currencyList.count)")
            return currencyList
        } catch {
            NSLog(error.localizedDescription)
        }
        return currencyList
    }
    
    func clearStorage() {
        NSLog("XYZ clearStorage START")
        currencyList = currencyList.count == 0 ? getEntityList() : currencyList
        for entity in currencyList {
            coreDataContext.performAndWait {
                coreDataContext.delete(entity)
            }
        }
        coreDataContext.performAndWait {
            if coreDataContext.hasChanges {
                do {
                    try coreDataContext.save()
                } catch {
                    NSLog(error.localizedDescription)
                }
                NSLog("XYZ clearStorage ENTITY DELETED")
            }
        }
        NSLog("XYZ clearStorage END")
    }
}
