//
//  TimerUseCase.swift
//  CurrencyConverter
//
//  Created by Taher on 29/7/23.
//

import Foundation

final class TimerUseCase: TimerUseCaseProtocol {

    var minimumIntervalTimeInMinute: Int {
        AppConstant.minimumIntervalTimeInMinute
    }

    var intervalStorageKey: String {
        AppConstant.intervalStorageKey
    }

    let repository: TimerRepositoryProtocol

    init(timerRepository: TimerRepositoryProtocol) {
        repository = timerRepository
    }

    func isValidTimeToFetchData() -> Bool {
        guard let lastUpdatedTimeInSeconds = repository.getInterval(
            key: intervalStorageKey
        ) else {
            return true
        }
        let currentTimeInSecond = getCurrentTimeInSeconds()
        let minutesDifference = Double((currentTimeInSecond - lastUpdatedTimeInSeconds)) / 60.0
        return minutesDifference >= Double(minimumIntervalTimeInMinute)
    }

    func saveDataUpdateTime(lastUpdateTimeIntervalInSeconds: Int?) {
        repository.saveInterval(
            key: intervalStorageKey,
            value: lastUpdateTimeIntervalInSeconds ?? getCurrentTimeInSeconds()
        )
    }

    func deleteDataUpdateTime() {
        repository.deleteInterval(key: intervalStorageKey)
    }

    private func getCurrentTimeInSeconds() -> Int {
        let currentTime = Date().timeIntervalSince1970
        return Int(currentTime)
    }
}
