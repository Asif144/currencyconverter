//
//  CurrencyConversionUseCase.swift
//  CurrencyConverter
//
//  Created by Taher on 29/7/23.
//

import Foundation

struct CurrencyConversionUseCase: CurrencyConversionUseCaseProtocol {

    func getNewCurrencyList(
        inputNumber: Double,
        baseCurrency: CurrencyUiModel,
        allCurrency: [CurrencyEntity],
        currencyToCountryMap: [String : String]
    ) -> [CurrencyUiModel] {
        var currencyList = [CurrencyUiModel]()
        for currency in allCurrency {
            if currency.currencyCode == baseCurrency.currencyCode {
                continue
            }
            let newValue = getConvertedValue(
                inputNumber: inputNumber,
                baseCurrencyValue: baseCurrency.currencyValue,
                otherCurrencyValue: currency.currencyValue
            )
            if let code = currency.currencyCode {
                let entity = CurrencyUiModel(
                    currencyCode: code,
                    currencyValue: newValue,
                    countryName: currencyToCountryMap[code] ?? ""
                )
                currencyList.append(entity)
            }
        }
        return currencyList.sorted { $0.currencyCode < $1.currencyCode }
    }
    
    func getConvertedUIModel(
        currencyApiData: CurrencyApiModel,
        currencyToCountryMap: [String : String]
    ) -> [CurrencyUiModel] {
        var currencyList = [CurrencyUiModel]()
        for (currencyCode, currencyValue) in currencyApiData.rates {
            let entity = CurrencyUiModel(
                currencyCode: currencyCode,
                currencyValue: currencyValue,
                countryName: currencyToCountryMap[currencyCode] ?? ""
            )
            currencyList.append(entity)
        }
        currencyList.sort { $0.currencyCode < $1.currencyCode }
        return currencyList
    }

    private func getConvertedValue(
        inputNumber: Double,
        baseCurrencyValue: Double,
        otherCurrencyValue: Double
    ) -> Double {
        return (otherCurrencyValue / baseCurrencyValue) * inputNumber
    }
}
