//
//  Typealias.swift
//  CurrencyConverter
//
//  Created by Taher on 29/7/23.
//

import Combine

typealias Observable<T> = AnyPublisher<T, Error>
typealias HTTPHeaders = [String: String]
typealias CurrencyEntityType = [String: Double]
typealias HTTPRequestParameters = [String: AnyHashable]
