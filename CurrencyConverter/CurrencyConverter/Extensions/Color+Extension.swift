//
//  Color+Extension.swift
//  CurrencyConverter
//
//  Created by Taher on 26/7/23.
//

import SwiftUI

extension Color {

    static let purpleBorder = Color(red: 142/255, green: 68/255, blue: 173/255)
}
