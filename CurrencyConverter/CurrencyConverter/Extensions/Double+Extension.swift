//
//  Double+Extension.swift
//  CurrencyConverter
//
//  Created by Taher on 31/7/23.
//

import Foundation

extension Double {

    static let maximumFractionDigits = 3
    static let currencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 1
        formatter.maximumFractionDigits = Double.maximumFractionDigits
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        return formatter
    }()

    var currencyFormatted: String {
        let zeroString = "0.0"
        let formatter = Double.currencyFormatter
        let number = self as NSNumber
        guard let currencyString = formatter.string(from: number) else {
            return zeroString
        }
        guard currencyString == zeroString else {
            return currencyString
        }
        var newCurrency = ""
        for i in 4...15 {
            formatter.maximumFractionDigits = i
            newCurrency = formatter.string(from: number)!
            if(newCurrency != zeroString) {
                formatter.maximumFractionDigits = i + 1
                newCurrency = formatter.string(from: number)!
                break
            }
        }
        formatter.maximumFractionDigits = Double.maximumFractionDigits
        return newCurrency
    }
}
