//
//  URLRequest+Extension.swift
//  CurrencyConverter
//
//  Created by Taher on 29/7/23.
//

import Foundation

extension URLRequest {

    enum HttpMethod: String {

        case get = "GET"
        case post = "POST"

        var current: String {
            rawValue
        }
    }
}
