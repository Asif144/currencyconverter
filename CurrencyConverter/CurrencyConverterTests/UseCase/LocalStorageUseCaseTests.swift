//
//  LocalStorageUseCaseTests.swift
//  CurrencyConverterTests
//
//  Created by Taher on 1/8/23.
//

import CoreData
import XCTest
@testable import CurrencyConverter

final class LocalStorageUseCaseTests: XCTestCase {

    var sut : LocalStorageUseCase!

    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = LocalStorageUseCase(
            persistenceController: PersistenceController.testPersistenceController
        )
    }

    override func tearDownWithError() throws {
        sut.clearStorage()
        sut = nil
        try super.tearDownWithError()
    }

    func testWhenSaveEntityCalledWithDataEntitiesShouldBeSaved() throws {
        sut.saveEntity(entities: DummyData.currencyRates)
        let entities = sut.getEntityList()
        XCTAssertFalse(entities.isEmpty)
    }

    func testGetEntityListCalledSavedEntitiesShouldBeReturned() throws {
        sut.saveEntity(entities: DummyData.currencyRates)
        let entities = sut.getEntityList()
        XCTAssertTrue(entities.count == DummyData.currencyRates.count)
    }

    func testWhenClearStorageCalleAllEntitiesShouldBeRemoved() throws {
        sut.saveEntity(entities: DummyData.currencyRates)
        var entities = sut.getEntityList()
        XCTAssertFalse(entities.isEmpty)
        sut.clearStorage()
        entities = sut.getEntityList()
        XCTAssertTrue(entities.isEmpty)
    }
}
