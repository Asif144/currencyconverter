//
//  TimerUseCaseTests.swift
//  CurrencyConverterTests
//
//  Created by Taher on 1/8/23.
//

import XCTest
@testable import CurrencyConverter

final class TimerUseCaseTests: XCTestCase {

    var sut : TimerUseCase!
    var mockTimerRepostory : MockTimerRepository!

    override func setUpWithError() throws {
        try super.setUpWithError()
        mockTimerRepostory = MockTimerRepository()
        sut = TimerUseCase(timerRepository: mockTimerRepostory)
    }

    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }

    func testWhenIsValidTimeToFetchDataCalledWithNoPreviousTimeExistItShouldReturnTrue() throws {
        sut.deleteDataUpdateTime()
        XCTAssertTrue(sut.isValidTimeToFetchData())
    }

    func testWhenIsValidTimeToFetchDataCalledWithPreviousTimeExistItShouldReturnTrueForValidMinuteDifference() throws {
        let oldTime = getCurrentTimeInSeconds() - ((AppConstant.minimumIntervalTimeInMinute + 1) * 60)
        sut.saveDataUpdateTime(lastUpdateTimeIntervalInSeconds: oldTime)
        XCTAssertTrue(sut.isValidTimeToFetchData())
    }

    func testWhenIsValidTimeToFetchDataCalledWithPreviousTimeExistItShouldReturnFalseForInValidMinuteDifference() throws {
        let oldTime = getCurrentTimeInSeconds() - ((AppConstant.minimumIntervalTimeInMinute - 1) * 60)
        sut.saveDataUpdateTime(lastUpdateTimeIntervalInSeconds: oldTime)
        XCTAssertFalse(sut.isValidTimeToFetchData())
    }

    private func getCurrentTimeInSeconds() -> Int {
        let currentTime = Date().timeIntervalSince1970
        return Int(currentTime)
    }
}
