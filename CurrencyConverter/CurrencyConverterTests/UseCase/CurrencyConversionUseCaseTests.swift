//
//  CurrencyConversionUseCaseTests.swift
//  CurrencyConverterTests
//
//  Created by Taher on 1/8/23.
//

import CoreData
import XCTest
@testable import CurrencyConverter

final class CurrencyConversionUseCaseTests: XCTestCase {

    var sut : CurrencyConversionUseCase!
    var localStorage : LocalStorageUseCase!

    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = CurrencyConversionUseCase()
        localStorage = LocalStorageUseCase(
            persistenceController: PersistenceController.testPersistenceController
        )
        localStorage.saveEntity(entities: DummyData.currencyRates)
    }

    override func tearDownWithError() throws {
        localStorage.clearStorage()
        sut = nil
        localStorage = nil
        try super.tearDownWithError()
    }

    func testWhenGetNewCurrencyListCalledItShouldReturnNewConvertedValues() throws {
        let newBase = "USD"
        let oldList = sut.getNewCurrencyList(
            inputNumber: 1.0,
            baseCurrency: CurrencyUiModel(currencyCode: newBase, currencyValue: 1.0, countryName: ""),
            allCurrency: localStorage.getEntityList(),
            currencyToCountryMap: DummyData.currencyToCountry
        )
        let newList = sut.getNewCurrencyList(
            inputNumber: 10.0,
            baseCurrency: CurrencyUiModel(currencyCode: newBase, currencyValue: 1.0, countryName: ""),
            allCurrency: localStorage.getEntityList(),
            currencyToCountryMap: DummyData.currencyToCountry
        )
        for (oldData, newData) in zip(oldList, newList) {
            XCTAssertTrue(Int(oldData.currencyValue) == Int(newData.currencyValue / 10))
        }
    }

    func testWhenGetNewCurrencyListCalledItShouldReturnNewConvertedValuesExceptBase() throws {
        let newBase = "USD"
        let newList = sut.getNewCurrencyList(
            inputNumber: 10.0,
            baseCurrency: CurrencyUiModel(currencyCode: newBase, currencyValue: 1.0, countryName: ""),
            allCurrency: localStorage.getEntityList(),
            currencyToCountryMap: DummyData.currencyToCountry
        )
        XCTAssertFalse(newList.contains(where: { $0.currencyCode == newBase }))
    }

    func testWhenGetConvertedUIModelIsCalledItShouldReturnUiPresentableData() {
        let uiData = sut.getConvertedUIModel(
            currencyApiData: CurrencyApiModel.createDummy(rates: DummyData.currencyRates),
            currencyToCountryMap: DummyData.currencyToCountry
        )
        XCTAssertTrue(uiData.count > 0)
        XCTAssert((uiData as Any) is [CurrencyUiModel])
    }
}
