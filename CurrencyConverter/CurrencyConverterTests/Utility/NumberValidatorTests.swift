//
//  CurrencyConverterTests.swift
//  CurrencyConverterTests
//
//  Created by Taher on 26/7/23.
//

import XCTest
@testable import CurrencyConverter

final class NumberValidatorTests: XCTestCase {

    var sut: NumberValidator!

    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = NumberValidator()
    }

    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }

    func testPattern() throws {
        XCTAssertTrue(sut.isValidPositiveNumber(number: "5.5"))
        XCTAssertTrue(sut.isValidPositiveNumber(number: "0.5"))
        XCTAssertTrue(sut.isValidPositiveNumber(number: "5"))
        XCTAssertTrue(sut.isValidPositiveNumber(number: "15.5"))
        XCTAssertTrue(sut.isValidPositiveNumber(number: "5.005"))
        XCTAssertTrue(sut.isValidPositiveNumber(number: "5.0"))
        XCTAssertTrue(sut.isValidPositiveNumber(number: ".5"))
        XCTAssertFalse(sut.isValidPositiveNumber(number: "-5.5"))
        XCTAssertFalse(sut.isValidPositiveNumber(number: "5."))
        XCTAssertFalse(sut.isValidPositiveNumber(number: "0"))
        XCTAssertFalse(sut.isValidPositiveNumber(number: "abc"))
        XCTAssertFalse(sut.isValidPositiveNumber(number: "123.45+", pattern:  #"^(\d+)$"#))
    }

    func testLength() throws {
        XCTAssertTrue(sut.isValidLength(number: "5.126"))
        XCTAssertFalse(sut.isValidLength(number: "5.12646765"))
    }
}
