//
//  NetworkDataSourceTests.swift
//  CurrencyConverterTests
//
//  Created by Taher on 1/8/23.
//

import XCTest
import Combine
@testable import CurrencyConverter

final class NetworkDataSourceTests: XCTestCase {

    var sut : NetworkDataSource!
    var currencyApiData: CurrencyApiModel?
    var cancellables = Set<AnyCancellable>()
    let baseUrl = URL(string: "https://www.dummy.com/latest.json")

    override func setUpWithError() throws {
        try super.setUpWithError()
    }

    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }

    func testWhenPerformRequestCalledWithValidResponseProvidedItShouldReturnData() throws {
        guard let url = baseUrl else {
            return
        }
        let apiData = CurrencyApiModel.createDummy(rates: DummyData.currencyRates)
        var mockData = Data()
        let encoder = JSONEncoder()
        do {
            mockData = try encoder.encode(apiData)
        } catch {
            NSLog(error.localizedDescription)
        }
        let mockSession = MockSession.make(
            url: url,
            data: mockData,
            statusCode: 200
        )
        sut = NetworkDataSource(session: mockSession)
        let expectation = XCTestExpectation(
            description: "waitPerformRequestCalledWithValidResponseProvidedItShouldReturnData"
        )
        executeRequest()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 6.0)
        XCTAssertNotNil(currencyApiData)
    }

    func testWhenPerformRequestCalledWithInvalidResponseProvidedItShouldNotReturnData() throws {
        guard let url = baseUrl else {
            return
        }
        let mockSession = MockSession.make(
            url: url,
            data: Data("invalid data".utf8),
            statusCode: 401
        )
        sut = NetworkDataSource(session: mockSession)
        let expectation = XCTestExpectation(
            description: "waitPerformRequestCalledWithInvalidResponseProvidedItShouldNotReturnData"
        )
        executeRequest()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 6.0)
        XCTAssertNil(currencyApiData)
    }

    private func executeRequest() {
        sut.performRequest(
            APIRequestConfigurations.currencyData,
            for: CurrencyApiModel.self
        ).sink(
            receiveCompletion: { [weak self] completion in
                switch completion {
                case .failure:
                    self?.currencyApiData = nil
                case .finished: break
                }
            },
            receiveValue: { [weak self] data in
                self?.currencyApiData = data
            }
        )
        .store(in: &cancellables)
    }
}
