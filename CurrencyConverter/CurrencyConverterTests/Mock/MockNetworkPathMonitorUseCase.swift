//
//  MockNetworkPathMonitorUseCase.swift
//  CurrencyConverterTests
//
//  Created by Taher on 1/8/23.
//

import Foundation
import Network
@testable import CurrencyConverter

final class MockNetworkPathMonitorUseCase : NetworkPathMonitorUseCaseProtocol {

    var pathUpdateHandler: ((NWPath.Status) -> Void)?
    var currentNetworkStatus : NWPath.Status

    var isStartCalled = false
    var isCancelCalled = false

    init(currentNetworkStatus : NWPath.Status = .unsatisfied) {
        self.currentNetworkStatus = currentNetworkStatus
    }

    func start(queue: DispatchQueue) {
        isStartCalled = true
        pathUpdateHandler?(currentNetworkStatus)
    }

    func cancel() {
        isCancelCalled = true
    }
}
