//
//  MockSession.swift
//  CurrencyConverterTests
//
//  Created by Taher on 1/8/23.
//

import Foundation

enum MockSession {

    static func make(url: URL, data: Data, statusCode: Int) -> URLSession {
        let response = HTTPURLResponse(
            url: url,
            statusCode: statusCode,
            httpVersion: nil,
            headerFields: nil
        )
        let mockResponse = MockResponse(response: response ?? URLResponse(), url: url, data: data)
        MockURLProtocol.testResponses = [url: mockResponse]

        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [MockURLProtocol.self]

        return URLSession(configuration: config)
    }
}
