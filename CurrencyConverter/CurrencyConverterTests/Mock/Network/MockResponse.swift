//
//  MockResponse.swift
//  CurrencyConverterTests
//
//  Created by Taher on 1/8/23.
//

import Foundation

struct MockResponse {

    let response: URLResponse
    let url: URL
    let data: Data?
}
