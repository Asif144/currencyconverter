//
//  MockCurrencyConversionUseCase.swift
//  CurrencyConverterTests
//
//  Created by Taher on 1/8/23.
//

import Foundation
@testable import CurrencyConverter

final class MockCurrencyConversionUseCase : CurrencyConversionUseCaseProtocol {

    var dummyList : [CurrencyUiModel] = []
    var isGetNewCurrencyListCalled = false
    var isGetConvertedUIModelCalled = false

    func getNewCurrencyList(
        inputNumber: Double,
        baseCurrency: CurrencyUiModel,
        allCurrency: [CurrencyEntity],
        currencyToCountryMap: [String : String]
    ) -> [CurrencyUiModel] {
        isGetNewCurrencyListCalled = true
        return dummyList
    }

    func getConvertedUIModel(
        currencyApiData: CurrencyApiModel,
        currencyToCountryMap: [String : String]
    ) -> [CurrencyUiModel] {
        isGetConvertedUIModelCalled = true
        return dummyList
    }
}
