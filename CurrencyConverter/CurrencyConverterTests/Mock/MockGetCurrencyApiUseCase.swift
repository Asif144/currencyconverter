//
//  MockGetCurrencyApiUseCase.swift
//  CurrencyConverterTests
//
//  Created by Taher on 1/8/23.
//

import Foundation
import Combine
@testable import CurrencyConverter

final class MockGetCurrencyApiUseCase : GetCurrencyApiUseCaseProtocol {

    var shouldReturnData = false
    var isGetApiDataCalled = false

    func getApiData() -> CurrencyConverter.Observable<CurrencyApiModel> {
        isGetApiDataCalled = true
        if shouldReturnData {
            let dummy = CurrencyApiModel.createDummy(rates: DummyData.currencyRates)
            return Just(dummy)
                .setFailureType(to: Error.self)
                .eraseToAnyPublisher()
        }
        return Fail(error: HTTPError.invalidUrl)
            .eraseToAnyPublisher()
    }
}
