//
//  MockStaticDataUseCase.swift
//  CurrencyConverterTests
//
//  Created by Taher on 1/8/23.
//

import Foundation
@testable import CurrencyConverter

final class MockStaticDataUseCase : StaticDataUseCaseProtocol {

    func getCountryName(for currencyCode: String) -> String {
        return currencyCode
    }

    func getAllCurrencyCode() -> [String] {
        DummyData.allCurrencyCode
    }

    func getCurrencyToCountryMap() -> [String : String] {
        DummyData.currencyToCountry
    }
}
