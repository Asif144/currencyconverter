//
//  MockNetworkDataSource.swift
//  CurrencyConverterTests
//
//  Created by Taher on 1/8/23.
//

import Foundation
import Combine
@testable import CurrencyConverter


final class MockNetworkDataSource : NetworkDataSourceProtocol {

    var shouldReturnData = false
    var isGetApiDataCalled = false

    func performRequest<T>(_ configuration: RequestConfiguration, for type: T.Type)
    -> Observable<T> where T : Decodable {
        if(!shouldReturnData) {
            return Fail(error: HTTPError.invalidResponse)
                .eraseToAnyPublisher()
        }
        guard let data = CurrencyApiModel.createDummy(rates: DummyData.currencyRates) as? T else {
            return Fail(error: HTTPError.invalidResponse)
                .eraseToAnyPublisher()
        }
        return Just(data)
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
    }
}
