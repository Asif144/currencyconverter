//
//  MockTimerUseCase.swift
//  CurrencyConverterTests
//
//  Created by Taher on 1/8/23.
//

import Foundation
@testable import CurrencyConverter

final class MockTimerUseCase : TimerUseCaseProtocol {

    var minimumIntervalTimeInMinute: Int {
        10
    }

    var intervalStorageKey: String {
        "dummyKey"
    }

    var isSaveDataUpdateTimeCalled = false
    var isDeleteDataUpdateTimeCalled = false
    var isValidTime = false

    func isValidTimeToFetchData() -> Bool {
        isValidTime
    }

    func saveDataUpdateTime(lastUpdateTimeIntervalInSeconds: Int?) {
        isSaveDataUpdateTimeCalled = true
    }

    func deleteDataUpdateTime() {
        isDeleteDataUpdateTimeCalled = true
    }
}
