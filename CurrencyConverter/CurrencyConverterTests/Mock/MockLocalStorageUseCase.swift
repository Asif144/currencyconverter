//
//  MockLocalStorageUseCase.swift
//  CurrencyConverterTests
//
//  Created by Taher on 1/8/23.
//

import Foundation
import CoreData
@testable import CurrencyConverter

final class MockLocalStorageUseCase : LocalStorageUseCaseProtocol {

    var isSaveEntityCalled = false
    var isGetEntityListCalled = false
    var isClearStorageCalled = false
    var shouldReturnData = false
    var dummyEntity = [CurrencyEntity]()
    private let coreDataContext: NSManagedObjectContext

    init(persistenceController: PersistenceController) {
        coreDataContext = persistenceController.container.newBackgroundContext()
    }

    func saveEntity(entities: CurrencyEntityType) {
        isSaveEntityCalled = true
    }

    func getEntityList() -> [CurrencyEntity] {
        isGetEntityListCalled = true
        return shouldReturnData ? dummyEntity : []
    }

    func clearStorage() {
        isClearStorageCalled = true
    }

    func insertDummy(code: String, value: Double) {
        shouldReturnData = true
        let entity = CurrencyEntity(context: coreDataContext)
        entity.currencyCode = code
        entity.currencyValue = value
        dummyEntity.append(entity)
    }
}
