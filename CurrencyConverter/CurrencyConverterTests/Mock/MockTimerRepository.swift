//
//  MockTimerRepository.swift
//  CurrencyConverterTests
//
//  Created by Taher on 1/8/23.
//

import Foundation
@testable import CurrencyConverter

final class MockTimerRepository : TimerRepositoryProtocol {

    var dummyStorage = [String : Int]()
    func saveInterval(key: String, value: Int) {
        dummyStorage[key] = value
    }

    func getInterval(key: String) -> Int? {
        return dummyStorage[key]
    }

    func deleteInterval(key: String) {
        dummyStorage[key] = nil
    }
}
