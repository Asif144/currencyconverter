//
//  CurrencyApiRepositoryTests.swift
//  CurrencyConverterTests
//
//  Created by Taher on 1/8/23.
//

import XCTest
import Combine
@testable import CurrencyConverter

final class CurrencyApiRepositoryTests: XCTestCase {

    var sut: CurrencyApiRepository!
    var mockNetwork: MockNetworkDataSource!
    var isDataReceived: Bool!
    var cancellables = Set<AnyCancellable>()

    override func setUpWithError() throws {
        try super.setUpWithError()
        isDataReceived = false
        mockNetwork = MockNetworkDataSource()
        sut = CurrencyApiRepository(network: mockNetwork)
    }

    override func tearDownWithError() throws {
        mockNetwork = nil
        sut = nil
        try super.tearDownWithError()
    }

    func testWhenNetworkReturnsData() {
        mockNetwork.shouldReturnData = true
        executeRequest()
        let expectation = XCTestExpectation(description: "waitNetworkReturnsData")
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 7.0)
        XCTAssertTrue(isDataReceived)
    }

    func testWhenNetworkReturnsError() {
        mockNetwork.shouldReturnData = false
        executeRequest()
        let expectation = XCTestExpectation(description: "waitNetworkReturnsError")
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 7.0)
        XCTAssertFalse(isDataReceived)
    }

    private func executeRequest() {
        sut.getCurrencyData().sink(
            receiveCompletion: { [weak self] completion in
                switch completion {
                case let .failure(error):
                    NSLog("ERROR: \(error.localizedDescription)")
                    self?.isDataReceived = false
                case .finished:
                    NSLog("Login token response finished")
                }
            },
            receiveValue: { [weak self] _ in
                self?.isDataReceived = true
            }
        )
        .store(in: &cancellables)
    }
}


