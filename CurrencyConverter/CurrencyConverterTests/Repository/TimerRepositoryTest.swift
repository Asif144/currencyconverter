//
//  TimerRepositoryTest.swift
//  CurrencyConverterTests
//
//  Created by Taher on 1/8/23.
//

import XCTest
@testable import CurrencyConverter

final class TimerRepositoryTest: XCTestCase {

    var sut: TimerRepository!
    let testKey = "testKey"
    let unknownKey = "unknownKey"

    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = TimerRepository()
    }

    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }

    func testSaveInterval() throws {
        sut.saveInterval(key: testKey, value: 10)
        XCTAssertTrue(sut.storage.integer(forKey: testKey) == 10)
    }

    func testGetIntervalWithUnkonwnKey() throws {
        XCTAssertNil(sut.getInterval(key: unknownKey))
    }

    func testGetIntervalWithKonwnKey() throws {
        sut.saveInterval(key: testKey, value: 10)
        XCTAssertNotNil(sut.getInterval(key: testKey))
    }

    func testDeleteInterval() throws {
        sut.deleteInterval(key: testKey)
        XCTAssertNil(sut.getInterval(key: testKey))
    }
}
