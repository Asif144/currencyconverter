//
//  Double+ExtensionTests.swift
//  CurrencyConverterTests
//
//  Created by Taher on 1/8/23.
//

import XCTest
@testable import CurrencyConverter

final class DoubleExtensionTests: XCTestCase {

    var sut: Double!

    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = Double()
    }

    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }

    func testCurrencyFormat() throws {
        var testData : Double = 4.5
        XCTAssertTrue(testData.currencyFormatted == "4.5")

        testData = 0.000056
        XCTAssertTrue(testData.currencyFormatted == "0.00006")

        testData = 0.0
        XCTAssertTrue(testData.currencyFormatted == "0.0")

        testData = 4.146789
        XCTAssertTrue(testData.currencyFormatted == "4.147")

        testData = 123456789
        XCTAssertTrue(testData.currencyFormatted == "123,456,789.0")
    }
}
