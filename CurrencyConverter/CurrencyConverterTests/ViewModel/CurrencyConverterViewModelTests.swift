//
//  CurrencyConverterViewModelTests.swift
//  CurrencyConverterTests
//
//  Created by Taher on 1/8/23.
//

import XCTest
import Network
@testable import CurrencyConverter

final class CurrencyConverterViewModelTests: XCTestCase {

    var sut: CurrencyConverterViewModel!
    var numberValidator: NumberValidator!
    var mockLocalStorageUseCase: MockLocalStorageUseCase!
    var mockGetCurrencyApiUseCase: MockGetCurrencyApiUseCase!
    var mockTimerUseCase: MockTimerUseCase!
    var mockCurrencyConversionUseCase: MockCurrencyConversionUseCase!
    var mockStaticDataUseCase: MockStaticDataUseCase!
    var mockNetworkPathMonitorUseCase: MockNetworkPathMonitorUseCase!

    override func setUpWithError() throws {
        try super.setUpWithError()
        numberValidator = NumberValidator()
        mockLocalStorageUseCase = MockLocalStorageUseCase(
            persistenceController: PersistenceController.testPersistenceController
        )
        mockGetCurrencyApiUseCase = MockGetCurrencyApiUseCase()
        mockTimerUseCase = MockTimerUseCase()
        mockCurrencyConversionUseCase = MockCurrencyConversionUseCase()
        mockStaticDataUseCase = MockStaticDataUseCase()
        mockNetworkPathMonitorUseCase =  MockNetworkPathMonitorUseCase()
        sut = CurrencyConverterViewModel(
            numberValidator: numberValidator,
            currentState: .splash,
            localStorageUseCase: mockLocalStorageUseCase,
            getCurrencyApiUseCase: mockGetCurrencyApiUseCase,
            timerUseCase: mockTimerUseCase,
            currencyConversionUseCase: mockCurrencyConversionUseCase,
            staticDataUseCase: mockStaticDataUseCase,
            networkPathMonitorUseCase: mockNetworkPathMonitorUseCase
        )
    }

    override func tearDownWithError() throws {
        numberValidator = nil
        mockLocalStorageUseCase = nil
        mockGetCurrencyApiUseCase = nil
        mockTimerUseCase = nil
        mockCurrencyConversionUseCase = nil
        mockStaticDataUseCase = nil
        mockNetworkPathMonitorUseCase = nil
        sut = nil
        try super.tearDownWithError()
    }

    func testInitialState() throws {
        XCTAssertTrue(sut.currentState == .splash)
    }

    func testWhenTimerIsValidInternetIsConnectedShouldFetchApiData() {
        mockNetworkPathMonitorUseCase.currentNetworkStatus = .satisfied
        mockTimerUseCase.isValidTime = true

        let expectation = XCTestExpectation(description: "waitTimerIsValidInternetIsConnectedShouldFetchApiData")
        mockNetworkPathMonitorUseCase.start(queue: DispatchQueue.global())
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 6.0)
        XCTAssertTrue(mockGetCurrencyApiUseCase.isGetApiDataCalled)
    }

    func testWhenApiReturnDataLocalStorageShouldBeUpdated() {
        mockNetworkPathMonitorUseCase.currentNetworkStatus = .satisfied
        mockTimerUseCase.isValidTime = true
        mockGetCurrencyApiUseCase.shouldReturnData = true
        mockCurrencyConversionUseCase.dummyList = DummyData.uiModelList

        let expectation = XCTestExpectation(description: "waitApiReturnDataLocalStorageShouldBeUpdated")
        mockNetworkPathMonitorUseCase.start(queue: DispatchQueue.global())
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 6.0)
        XCTAssertTrue(mockLocalStorageUseCase.isClearStorageCalled)
        XCTAssertTrue(mockLocalStorageUseCase.isSaveEntityCalled)
    }

    func testWhenApiReturnDataTimerShouldBeUpdated() {
        mockNetworkPathMonitorUseCase.currentNetworkStatus = .satisfied
        mockTimerUseCase.isValidTime = true
        mockGetCurrencyApiUseCase.shouldReturnData = true
        mockCurrencyConversionUseCase.dummyList = DummyData.uiModelList

        let expectation = XCTestExpectation(description: "waitApiReturnDataTimerShouldBeUpdated")
        mockNetworkPathMonitorUseCase.start(queue: DispatchQueue.global())
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 6.0)
        XCTAssertTrue(mockTimerUseCase.isSaveDataUpdateTimeCalled)
    }

    func testWhenApiReturnErrorAnErrorPopUpShouldShown() {
        mockNetworkPathMonitorUseCase.currentNetworkStatus = .satisfied
        mockTimerUseCase.isValidTime = true
        mockGetCurrencyApiUseCase.shouldReturnData = false

        let expectation = XCTestExpectation(description: "waitApiReturnErrorAnErrorPopUpShouldShown")
        mockNetworkPathMonitorUseCase.start(queue: DispatchQueue.global())
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 6.0)
        XCTAssertTrue(sut.currentState == .loaded(LocalizedString.invalidResponse))
    }

    func testWhenTimerIsValidButNoInternetFoundShouldShowNoInternetError() {
        mockNetworkPathMonitorUseCase.currentNetworkStatus = .unsatisfied
        mockTimerUseCase.isValidTime = true
        let expectation = XCTestExpectation(description: "waitTimerIsValidButNoInternetFoundShouldShowNoInternetError")

        mockNetworkPathMonitorUseCase.start(queue: DispatchQueue.global())
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 6.0)
        XCTAssertTrue(sut.currentState == .loaded(LocalizedString.noInternet))
    }

    func testWhenTapOkOfNoInternetPopUpItShouldGone() {
        mockNetworkPathMonitorUseCase.currentNetworkStatus = .unsatisfied
        mockTimerUseCase.isValidTime = true
        let expectation = XCTestExpectation(description: "waitTapOkOfNoInternetPopUpItShouldGone")

        mockNetworkPathMonitorUseCase.start(queue: DispatchQueue.global())
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 6.0)
        XCTAssertTrue(sut.currentState == .loaded(LocalizedString.noInternet))
        sut.didTapOkButton()
        XCTAssertTrue(sut.currentState == .loaded(""))
    }

    func testWhenTimerIsInvalidButNoInternetFoundShouldShowNoError() {
        mockNetworkPathMonitorUseCase.currentNetworkStatus = .unsatisfied
        mockTimerUseCase.isValidTime = false
        mockNetworkPathMonitorUseCase.start(queue: DispatchQueue.global())

        let expectation = XCTestExpectation(description: "waitTimerIsInvalidButNoInternetFoundShouldShowNoError")
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 6.0)
        XCTAssertTrue(sut.currentState == .loaded(""))
    }

    func testWhenInvalidUserInputGivenShouldShowValidationErrorText() {
        sut.didUpdateInputText(inputText: "10A")
        XCTAssertTrue(sut.validationErrorText.count > 0)
        XCTAssertTrue(sut.validationErrorText == LocalizedString.insertValidPositiveNumber)

        sut.didUpdateInputText(inputText: "")
        XCTAssertTrue(sut.validationErrorText.count > 0)
        XCTAssertTrue(sut.validationErrorText == LocalizedString.insertValidPositiveNumber)
    }

    func testWhenUserInputExceedMaxLimitGivenShouldShowValidationErrorText() {
        sut.didUpdateInputText(inputText: "12345678")
        XCTAssertTrue(sut.validationErrorText.count > 0)
        XCTAssertTrue(sut.validationErrorText == LocalizedString.atMostCharacters(numberValidator.maxLength))
    }

    func testWhenValidUserInputGivenShouldShowNoError() {
        sut.didUpdateInputText(inputText: "100")
        XCTAssertTrue(sut.validationErrorText.count == 0)
    }

    func testWhenUserChangeCurrencyCodeUiDataShouldBeUpdated() {
        mockLocalStorageUseCase.insertDummy(code: "AED", value: 1.0)
        sut.didChangeCurrencyCode(newCode: "AED")
        XCTAssertTrue(mockCurrencyConversionUseCase.isGetNewCurrencyListCalled)
    }
}
