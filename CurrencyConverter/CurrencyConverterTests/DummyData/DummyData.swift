//
//  DummyData.swift
//  CurrencyConverterTests
//
//  Created by Taher on 1/8/23.
//

import Foundation
@testable import CurrencyConverter

final class  DummyData {
    
    static let currencyRates =  [
        "AED": 3.672538,
        "AFN": 66.809999,
        "USD": 1.0
    ]

    static let uiModelList : [CurrencyUiModel] = [
        CurrencyUiModel(currencyCode: "AED", currencyValue: 3.672538, countryName:  "United Arab Emirates Dirham"),
        CurrencyUiModel(currencyCode: "AFN", currencyValue: 66.809999, countryName:  "Afghan Afghani"),
        CurrencyUiModel(currencyCode: "USD", currencyValue: 1, countryName:  "United States Dollar")
    ]

    static let allCurrencyCode = ["AED", "AFN", "USD"]

    static let currencyToCountry = [
        "AED": "United Arab Emirates Dirham",
        "AFN": "Afghan Afghani",
        "USD": "United States Dollar"
    ]
}
